                THE MODERNIZED MALCOMPLIANCE MITIGATION MANIFEST
                             Version 1, 11 May 2015

This document defines malcompliance in the #offtopia context and known instances
of it. This is a part of an effort to retain only the truly objectionable known
instances of malcompliance from earlier specifications. Some oddness is
preserved for backwards compatibility with historical contexts.

Channel regulars should familiarize themselves with this code for common
politeness and help shape it so it matches the commonly agreed-upon acceptable
behavior. This document is meant to minimize rude behavior.

§1. The principles of the git applies. Don't forget to pull the latest edition.

 a. Newspeaking is fun.

 b. UTF-8 (Unix line terminators, no BOM) is the one true encoding.

 c. Text is encoded in the one true encoding unless proven otherwise.

§2. Malcompliance is:

    * Circumventing the spirit of the rules while technically obeying.
    * Violating established community guidelines.
    * Opposing authority that is supposedly trying to help.

 a. Don't malcomply.

 b. If you malcomply, act in accordance with §3.b.

§3. If you malcomplied, that sucks, try to follow the guidelines or help improve
    them.

 a. If bots malcomply, the bot owner shall immediately rectify the malcompliant
    behavior upon notification.

 b. If you feel guilty and want to punish yourself, kick yourself or otherwise
    disconnect any way you like.

 c. If you can't seem to act properly despite feeling guilty, recognize your
    inability to comply and go do something else instead.

§4. No inventing new ways to spam.

 !. No spam!!!

 *. No linear combinations of §4 subsections.

 -. N-o s-p-a-m.

 a. No join-spam.

 b. No rename-spam.

 c. No spam-spam.

 d. No flooding.

 e. No massively repeating the nicks of channel regulars.

 f. No kick-spam.

 g. No ban-spam.

 h. No giving ops-spam.

 i. No name-list spam.

 j. No voice-spam.

 K. No caps-lock-spam.

 l. No bot-spam.

 m. No greeting-spam.

 n. No bye-spam.

 o. No recursion-spam.

 p. No permutate-spam.

 q. No poc-spam.

 r. No notice-spam.

 s. No ‘:)’ spam.

 t. No unicode-spam.

 u. No unsay-say.

 v. No Z̐̎̃͛͗ͮ̆a̾̇̊ͯl̃̒́ͬ̃͒͆̊ͭ́g̔ͥ̌̇͆ͧ̅̓̌ͩͪ͊o͐̑̿ͫ͂̊͒ͬ̀̋ͮ̋͑̑̚-ͩ̔̌̌ͪͤ̇ͧͬ̏ͧͥ̏s̓ͫ̈ͩ͗̂͂ͣp̓̾̿͂͒̚aͯͫ̃̿̆ͫ̑̊̀̔͌͋̏mͣ̾ͦ̾ͭ̇̂̇ͮ͗̈́.

 y. No why-spam.

 z. No nümberwangspäm.

 æ. No cats-on-keyboard-spam.

 §. No §spam.

 :D. Don't be clinically votehappy.

 D:. No D:-spam.

§5. If everyone else did it and it wasn't okay for them to do it, it's no excuse
    for you to do it too.

 a. You should tell bot owners if their bots malcomply.

 b. If it annoys you, point out someone else's malcompliance publicly or
    discretely.

 c. Unavailability of this document is no excuse for malcompliance.

§6. If you are malcomplying, and others are too, don't make it worse.

§9. Don't have operator status if you are not confident you can act properly. If
    you aren't confident, deop yourself.

§10. Respect all channel laws.

  a. If a channel law requires you to do unacceptable things, find some way to
     resolve the paradox.

§11. Don't make a specification that tries to govern the behavior of channel
     members unless they explicitly allow you to.

  a. Don't enforce such specifications.

§12. You must comply with the latest version of this document, including, but
     not limited to, documents brought to attention through time travel,
     precognition, git merges, or inevitability.

§13. Please help improve this document.

  a. You may send pull requests.

§14. Just get along, alright.

  a. Typos doesn't change the intent of sections in this document.

  b. If other people think you should feel guilty, and you don't, you might not
     be level headed. Take some time off if you want to.

§18. Be helpful.

§25. Kicks that points out malcompliance should cite the relevant sections of
     this document in this format.

     /kick malcompliance_defendant §23, §24

§26. Don't kick idlers.

§30. Don't send messages containing a substring that when converted to
     lower-case SHA1 hashes to this value:

     e3da05ed509d37955390ecc509e156ebe8369f94

  a. Point out when other people violate this. Consider asking them to kick
     themselves in accordance with §3.b.

  b. You don't want to know the string that hashes to that.

  c. Or, just ignore it when you see the string.

  d. No, you don't want to know it.

  e. Don't try to rephrase it either.

  f. Don't make others violate §30 or subsections.

  g. Don't search for the hash online lest you violate §30.d.

§31. Do your homework if you complain you should.

§37. Don't do wanton destruction of the channel topic.

§49. Don't do recursive remove with or without force on sortie.

§52. Learn LaTeX if you insist on using awful formats.

§58. People have the right to idle with ops if it is not forbidden for them to
     have ops.

§63. Don't post messages that contain dangerous shell commands. Sometimes sortie
     manages to paste it into this command line shell. No, really.

  a. Kick those that violate §63.

§73. Feel free to say goodbye. Niht is a popular and important word.

  a. Wait a few moments before disconnecting.

  b. Come back tomorrow.

§74. Feel free to say hi when you come back.

  a. Feel free to say hi if you've been away for a while.

  b. There's no need to say hi again when people hi you.

  c. Feel free to say hi when other people say hi not in response to you.

§75. It's recommended to enjoy these works:

  a. Nineteen Eighty-Four.

  b. The Hitchhiker's Guide to the Galaxy series.

  c. 2001: A Space Oddysey.

  d. Gödel, Escher, Bach.

  e. FireFly.

  f. Louie.

  g. Breaking Bad.

§82. Make things okay again if you made them not okay.

§84. Think carefully before trusting yourself to act sanely.

§88. More known instances of malcompliance can be forbidden in this document.

§89. You can exile yourself in protest.

  a. A §89 exile is imminent. Capitulate immediately.

§95. Don't try to circumvent mechanisms designed to avoid malcompliance.

§98. Don't needlessly expand the left margin of usernames for channel regulars.
     This can happen on notices.

§104. Don't abuse the relaxed IRC rules regarding what characters can be used in
      nicks, especially non-alphabetical characters.

   a. Nicks shall not be emoticons.

§105. Paths shall use forward slashs.

   a. Backslashes shall escape.

   b. Shell quoting shall apply.

§111. No join-voting lest unintended legislation.

§112. Don't betray trust.

§115. Stick to your main nick whenever possible.

§123. No illumanavyti math 6/3 = 2^3 HL3 confirmed N=NP <=> P=1 Elon Musk.

   a. No spelling it correctly or deducing it from axioms, either.

§128. Don't needlessly advertise #offtopia in #osdev.

§132. Sections are denoted with ‘§’ and not ‘$’.

§133. ISO 8601 is the desirable date format.

§137. Every bot can become minicat at any time for any reason or lack of reason.
